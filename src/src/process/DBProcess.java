package process;

import connection.DBConnection;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DBProcess {

    private static Connection con = DBConnection.getConnection();
   static private    ArrayList<Object> datas = new ArrayList<>();

    private static Statement statement = null;

    public static void createStudentTable(){
        try {
            String query = "CREATE TABLE student(studentId INT PRIMARY KEY NOT NULL, name VARCHAR(255), surname VARCHAR(255), birthYear INT, studentNumber VARCHAR(30))";
            statement = con.createStatement();
            statement.execute(query);
            System.out.println("Student Table has created successfully !");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        finally {
            try {
                statement.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        DBConnection.closeConnection();

    }

    public static void insertStudent(){


        try{
            String query = "INSERT INTO student_row_info(id)" +
                    "VALUES(1)";
            statement = con.createStatement();
            String query2 = "INSERT INTO student(id,name,surname,fee,start_date,leave_date,student_row_info_id,teacher_id ) " +
                        "VALUES(1,'X','Detroit',200,'2023-01-01', '2023-07-07', 1, 3)";
                statement= con.createStatement();
            statement.execute(query);
                statement.execute(query2);
                System.out.println("Data inserted succesfully !");


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }




    }

    public static void insertTeacher(){
        try {
            String query2 = "INSERT INTO teacher_row_info(id) " +
                    "VALUES(3)";
            statement = con.createStatement();
            statement.execute(query2);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


        try{
//            int teacher_row_info_id = 1;
            String query = "INSERT INTO teacher(id,name,surname,salary,start_date,leave_date,teacher_row_info_id)" +
                    "VALUES(3,'Pirvali','Ashrafov',500,'2022-01-03','2022-07-05',3)";
            statement = con.createStatement();
            statement.execute(query);


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }



    }

    public static void showStudent() {
        try {
            String query = "Select * from student";
            statement = con.createStatement();
            ResultSet result = statement.executeQuery(query);


            while (result.next()) {
                String data = "";
                for (int i = 1; i <= 8; i++) {
                    data+=result.getString(i) + " | ";
                }
                System.out.println(data);
                datas.add(data);
            }

//            for (int i = 0; i <datas.size() ; i++) {
//                System.out.println(datas.get(i));
//            }



        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
