package process;

import connection.DBConnection;

import java.sql.*;
import java.util.ArrayList;
import java.util.Scanner;

public class DBProcess2 {

    private static Connection con = DBConnection.getConnection();
    static private    ArrayList<Object> datas = new ArrayList<>();

    private static PreparedStatement preparedStatement = null;

    private static int studentID ;
    private static int teacherID ;
    public static Scanner scanner = new Scanner(System.in);

    public static void createStudentTable(){
        try {
            String query = "CREATE TABLE student(studentId INT PRIMARY KEY NOT NULL, name VARCHAR(255), surname VARCHAR(255), birthYear INT, studentNumber VARCHAR(30))";
            preparedStatement = con.prepareStatement(query);
            preparedStatement.execute();
            System.out.println("Student Table has created successfully !");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        DBConnection.closeConnection();

    }

    public static void insertStudent(){
        try {
            String query = "Select * from student";
            preparedStatement = con.prepareStatement(query);
            ResultSet result = preparedStatement.executeQuery(query);


            while (result.next()) {

                for (int i = 1; i <= 1; i++) {
                    studentID=result.getInt(i);
                }
            }
            studentID+=1;


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


        System.out.println("Student add");
        System.out.println("Ad");
        String name = scanner.next();
        System.out.println("Soyad");
        String surname = scanner.next();
        System.out.println("Fee");
        int fee = scanner.nextInt();
        System.out.println("Start Date - dd-MM-yyyy");
        String startDate = scanner.next();
        System.out.println("Leave Date - dd-MM-yyyy");
        String leaveDate = scanner.next();
        System.out.println("Teacher id");
        int teacherId = scanner.nextInt();


        try {
            String query = "INSERT INTO student_row_info(id)" +
                    "VALUES(?)";
            preparedStatement = con.prepareStatement(query);
            preparedStatement.setInt(1,studentID);
            System.out.println(" student row created");
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


        try{


            String query2 = "INSERT INTO student(id,name,surname,fee,start_date,leave_date,student_row_info_id,teacher_id ) " +
                    "VALUES(?,?,?,?,?,?,?,?)";
            preparedStatement= con.prepareStatement(query2);
            preparedStatement.setInt(1,studentID);
            preparedStatement.setString(2,name);
            preparedStatement.setString(3,surname);
            preparedStatement.setInt(4,fee);
            preparedStatement.setString(5,startDate);
            preparedStatement.setString(6,leaveDate);
            preparedStatement.setInt(7,studentID);
            preparedStatement.setInt(8,teacherId);


            preparedStatement.execute();
            System.out.println("Data inserted succesfully !");


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }




    }

    public static void insertTeacher(){
        try {
            String query = "Select * from teacher";
            preparedStatement = con.prepareStatement(query);
            ResultSet result = preparedStatement.executeQuery(query);


            while (result.next()) {

                for (int i = 1; i <= 1; i++) {
                    teacherID=result.getInt(i);
                }
            }
            teacherID+=1;





        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        try {
            String query2 = "INSERT INTO teacher_row_info(id) " +
                    "VALUES(?)";
            preparedStatement = con.prepareStatement(query2);
            preparedStatement.setInt(1,teacherID);
            preparedStatement.execute();
            System.out.println(" teacher row created");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


        try{
            System.out.println("Teacher add");
            System.out.println("Ad");
            String name = scanner.next();
            System.out.println("Soyad");
            String surname = scanner.next();
            System.out.println("Salary");
            int salary = scanner.nextInt();
            System.out.println("Start Date - dd-MM-yyyy");
            String startDate = scanner.next();
            System.out.println("Leave Date - dd-MM-yyyy");
            String leaveDate = scanner.next();


            String query = "INSERT INTO teacher(id,name,surname,salary,start_date,leave_date,teacher_row_info_id)" +
                    "VALUES(?,?,?,?,?,?,?)";
            preparedStatement = con.prepareStatement(query);
            preparedStatement.setInt(1,teacherID);
            preparedStatement.setString(2,name);
            preparedStatement.setString(3,surname);
            preparedStatement.setInt(4,salary);
            preparedStatement.setString(5,startDate);
            preparedStatement.setString(6,leaveDate);
            preparedStatement.setInt(7,teacherID);
            preparedStatement.execute();


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }



    }

    public static void showStudent() {
        try {
            String query = "Select * from student INNER JOIN teacher ON student.teacher_id=teacher.id;";
            preparedStatement = con.prepareStatement(query);
            ResultSet result = preparedStatement.executeQuery(query);


            while (result.next()) {
                String data = "";
                for (int i = 1; i <= 15; i++) {
                    if(i!=8){
                        data+=result.getString(i) + " | ";
                    }
//                    data+=result.getString(i) + " | ";
                }
                System.out.println(data);
                datas.add(data);
            }

//            for (int i = 0; i <datas.size() ; i++) {
//                System.out.println(datas.get(i));
//            }



        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static void showTeacher() {
        try {
            String query = "Select * from teacher";
            preparedStatement = con.prepareStatement(query);
            ResultSet result = preparedStatement.executeQuery(query);


            while (result.next()) {
                String data = "";
                for (int i = 1; i <= 7; i++) {
                    data+=result.getString(i) + " | ";
                }
                System.out.println(data);
            }

//            for (int i = 0; i <datas.size() ; i++) {
//                System.out.println(datas.get(i));
//            }



        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
